<?php
/*
 * Plugin Name: VLP Forms
 * Description: This plugin provides sipmlified tools to build GutenbergBlocks using ACF block registration.
 * Version: 0.0.1
 * Text Domain: vlp-forms
 */

defined('ABSPATH') or die('DIE!!!!');

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

use VLPForms\Main;

register_activation_hook(__FILE__, function () {
    if (!is_plugin_active('advanced-custom-fields/acf.php') && !is_plugin_active('advanced-custom-fields-pro/acf.php')) {
        deactivate_plugins(plugin_basename(__FILE__));
        wp_die('This plugin requires the ACF plugin to be installed and active');
    }

    $plugin = new Main();
    add_action('plugin_loaded', [$plugin, 'init']);
});
