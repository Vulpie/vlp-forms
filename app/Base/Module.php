<?php

namespace VLPForms\Base;

abstract class Module
{
    /**
     * Plugin directory path
     *
     * @var string
     */
    public $pluginFolder;

    /**
     * Plugin url path
     *
     * @var string
     */
    public $pluginURL;

    /**
     * Scripts folder url path
     *
     * @var string
     */
    public $scriptsDirURL;

    /**
     * Styles folder url path
     *
     * @var string
     */
    public $stylesDirURL;

    public function __construct()
    {
        $this->pluginFolder = dirname(__DIR__, 1);
        $this->pluginURL = plugin_dir_url(dirname(__FILE__));

        $this->scriptsDirURL = $this->pluginURL . 'assets/public/js/';
        $this->stylesDirURL = $this->pluginURL . 'assets/public/css/';
    }

    /**
     *
     * @return void
     */
    public function init(): void
    {
        add_action('init', [$this, 'register']);
    }

    /**
     *
     * @return void
     */
    public function register(): void
    {
    }

    /**
     *
     * @param string fileName
     * @param mixed[] attr
     * @return string
     */
    public function render(string $fileName, array $attr = []): string
    {
        $viewTemplatePath = $this->pluginFolder . '/views/' . $fileName . '.php';

        if (file_exists($viewTemplatePath)) {
            ob_start();
            extract($attr);
            require($viewTemplatePath);
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }

        return error_log($fileName . 'file not found.', 0);
    }
}
