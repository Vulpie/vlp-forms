<?php

namespace VLPForms;

use VLPForms\Base\Module;

class Main
{
    /**
     * 
     * @var Module[]
     */
    public $modules;

    public function __construct()
    {
        $this->modules = [];
    }

    /**
     * 
     * @return void
     */
    public function init(): void
    {
        $this->registerModules();
    }

    /**
     * 
     * @return void
     */
    public function registerModules(): void
    {
        foreach ($this->modules as $class) {
            $classInstance = new $class();
            $classInstance->init();
        }
    }
}
